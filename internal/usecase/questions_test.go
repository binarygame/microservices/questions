package usecase_test

import (
	"bytes"
	"context"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/questions/internal/usecase"
	"gitlab.com/binarygame/microservices/questions/internal/usecase/mocks"
	"gitlab.com/binarygame/microservices/questions/pkg/errdefs"
	model "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

func setupMocks() (
	*mocks.MockQuestionsRepository,
	*mocks.MockTelemetryService,
	*mocks.MockTelemetrySpan,
) {
	mockRepo := new(mocks.MockQuestionsRepository)
	mockTel := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)

	return mockRepo, mockTel, mockSpan
}

var (
	logBuffer bytes.Buffer
	logger    *slog.Logger = slog.New(slog.NewTextHandler(&logBuffer, nil))
)

func TestGenerateQuestionSet(t *testing.T) {
	qArr := []model.Question{
		&model.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: model.BinaryToDecimal},
	}
	qCount := len(qArr)

	t.Run("success", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("GetRandomQuestions", mock.Anything, model.Easy, mock.AnythingOfType("int64")).Return(qArr, nil)
		repo.On("SaveQuestionSet", mock.Anything, mock.Anything).Return(nil)
		repo.On("IsSeeded", context.TODO()).Return(true)
		tel.On("IncrementCounter", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return()

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GenerateQuestionSet(context.TODO(), int32(qCount), model.Easy)

		require.NoError(t, err)
		assert.Equal(t, resultSet.Level, model.Easy)
		assert.Equal(t, resultSet.Questions, qArr)
		assert.Len(t, resultSet.Questions, qCount)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database fail before save", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("IsSeeded", context.TODO()).Return(true)
		repo.On("GetRandomQuestions", mock.Anything, mock.Anything, mock.AnythingOfType("int64")).Return(nil, assert.AnError)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GenerateQuestionSet(context.TODO(), int32(qCount), model.Easy)

		assert.Nil(t, resultSet)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("question count out of range", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GenerateQuestionSet(context.TODO(), -1, model.Easy)

		assert.Nil(t, resultSet)
		assert.ErrorIs(t, err, errdefs.ErrQuestionCountOutOfRange)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("empty set", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("GetRandomQuestions", mock.Anything, mock.Anything, mock.AnythingOfType("int64")).Return([]model.Question{}, nil)
		repo.On("IsSeeded", context.TODO()).Return(true)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GenerateQuestionSet(context.TODO(), int32(qCount), model.Easy)

		assert.Nil(t, resultSet)
		assert.ErrorIs(t, err, errdefs.ErrNoQuestionsFound)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database fail on save", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("GetRandomQuestions", mock.Anything, model.Easy, mock.AnythingOfType("int64")).Return(qArr, nil)
		repo.On("SaveQuestionSet", mock.Anything, mock.Anything).Return(assert.AnError)
		repo.On("IsSeeded", context.TODO()).Return(true)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GenerateQuestionSet(context.TODO(), int32(qCount), model.Easy)

		assert.Nil(t, resultSet)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})
}

func TestGetQuestionSet(t *testing.T) {
	qArr := []model.Question{
		&model.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: model.BinaryToDecimal},
	}

	qSet := &model.QuestionSet{ID: "qs1", Level: model.Easy, Questions: qArr}

	t.Run("success", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("GetQuestionSet", mock.Anything, qSet.ID).Return(qSet, nil)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GetQuestionSet(context.TODO(), "qs1")

		require.NoError(t, err)
		assert.Equal(t, resultSet, qSet)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database fail", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("GetQuestionSet", mock.Anything, qSet.ID).Return(nil, assert.AnError)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GetQuestionSet(context.TODO(), "qs1")

		assert.Nil(t, resultSet)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("invalid data", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)

		uc := usecase.New(logger, repo, tel)
		resultSet, err := uc.GetQuestionSet(context.TODO(), "")

		assert.Nil(t, resultSet)
		assert.ErrorIs(t, err, errdefs.ErrQuestionSetIDEmpty)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})
}

func TestBuildDataset(t *testing.T) {
	t.Run("success not seeded", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("IsSeeded", mock.Anything).Return(false)
		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(nil).Times(3)
		repo.On("MarkAsSeeded", mock.Anything).Return(nil)
		tel.On("IncrementCounter", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int32")).Return()

		uc := usecase.New(logger, repo, tel)
		err := uc.BuildDataset(context.TODO(), 10)

		assert.NoError(t, err)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("success already seeded", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("IsSeeded", mock.Anything).Return(true)

		uc := usecase.New(logger, repo, tel)
		err := uc.BuildDataset(context.TODO(), 10)

		assert.NoError(t, err)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})

	t.Run("database fail", func(t *testing.T) {
		repo, tel, sp := setupMocks()
		tel.On("Record", mock.Anything, mock.Anything, mock.Anything).Return(sp)
		repo.On("IsSeeded", mock.Anything).Return(false)

		uc := usecase.New(logger, repo, tel)

		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(assert.AnError).Once()
		err := uc.BuildDataset(context.TODO(), 10)
		require.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(assert.AnError).Once()
		err = uc.BuildDataset(context.TODO(), 10)
		require.Error(t, err, errdefs.ErrDatabaseFailed)

		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(nil).Twice()
		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(assert.AnError).Once()
		err = uc.BuildDataset(context.TODO(), 10)
		require.Error(t, err, errdefs.ErrDatabaseFailed)

		repo.On("SaveLevelSet", mock.Anything, mock.Anything, mock.Anything).Return(nil).Times(3)
		repo.On("MarkAsSeeded", mock.Anything).Return(assert.AnError)
		err = uc.BuildDataset(context.TODO(), 10)
		require.Error(t, err, errdefs.ErrDatabaseFailed)

		repo.AssertExpectations(t)
		tel.AssertExpectations(t)
		sp.AssertExpectations(t)
	})
}
