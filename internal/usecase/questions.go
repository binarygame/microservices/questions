package usecase

import (
	"context"
	"log/slog"
	"math/rand"
	"strconv"

	"github.com/google/uuid"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/questions/internal/repository"
	"gitlab.com/binarygame/microservices/questions/pkg/constants"
	"gitlab.com/binarygame/microservices/questions/pkg/errdefs"
	model "gitlab.com/binarygame/microservices/questions/pkg/models/questions"

	questionTypes "gitlab.com/binarygame/microservices/questions/pkg/models/question_types"
)

type Questions interface {
	GenerateQuestionSet(ctx context.Context, questionCount int32, level model.QuestionSetLevel, shuffle bool) (*model.QuestionSet, error)
	GetQuestionSet(ctx context.Context, questionSetID string) (*model.QuestionSet, error)
	BuildDataset(ctx context.Context, numQuestions int32) error
}

type questions struct {
	logger     *slog.Logger
	repository repository.QuestionsRepository
	telemetry  telemetry.TelemetryService
}

func New(logger *slog.Logger, repo repository.QuestionsRepository, telemetry telemetry.TelemetryService) *questions {
	return &questions{
		logger:     logger,
		repository: repo,
		telemetry:  telemetry,
	}
}

func (qu *questions) GenerateQuestionSet(ctx context.Context, questionCount int32, level model.QuestionSetLevel, shuffle bool) (*model.QuestionSet, error) {
	// New grouped logger
	logger := qu.logger.WithGroup("generate_question_set").With(
		"questionCount", questionCount,
		"level", level,
	)

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "GenerateQuestionSet called")

	span := qu.telemetry.Record(ctx, "GenerateQuestionSet", map[string]string{
		"questionCount": strconv.Itoa(int(questionCount)),
		"level":         string(level),
	})
	defer span.End()

	if questionCount <= 0 {
		return nil, errdefs.ErrQuestionCountOutOfRange
	}

	// Add check if seeded in case repository gets reset
	if !qu.repository.IsSeeded(ctx) {
		logger.LogAttrs(ctx, slog.LevelInfo, "Database not seeded, trying to seed it")
		err := qu.BuildDataset(ctx, constants.NumGeneratedQuestions)
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Could not seed database", slog.Any("error", err))
			return nil, errdefs.ErrDatabaseFailed
		}
	}

	logger.LogAttrs(ctx, slog.LevelDebug, "Generating question set")

	questions, err := qu.repository.GetRandomQuestions(ctx, level, int64(questionCount))
	if err != nil {
		return nil, errdefs.ErrDatabaseFailed
	}

	if len(questions) == 0 {
		return nil, errdefs.ErrNoQuestionsFound
	}

	questionSetID := uuid.New().String()
	questionSet := model.QuestionSet{
		ID:        questionSetID,
		Level:     level,
		Questions: questions,
		Shuffle:   shuffle,
	}

	err = qu.repository.SaveQuestionSet(ctx, questionSet)
	if err != nil {
		return nil, errdefs.ErrDatabaseFailed
	}

	qu.telemetry.IncrementCounter(ctx, constants.SetsCreatedCounterKey, 1)

	return &questionSet, nil
}

func (qu *questions) GetQuestionSet(ctx context.Context, questionSetID string) (*model.QuestionSet, error) {
	// New grouped logger
	logger := qu.logger.WithGroup("get_question_set").With("questionSetID", questionSetID)

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "GetQuestionSet called")

	span := qu.telemetry.Record(ctx, "GetQuestionSet", map[string]string{
		"questionSetID": questionSetID,
	})
	defer span.End()

	if questionSetID == "" {
		return nil, errdefs.ErrQuestionSetIDEmpty
	}

	logger.LogAttrs(ctx, slog.LevelDebug, "Retrieving question set")

	questionSet, err := qu.repository.GetQuestionSet(ctx, questionSetID)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Error getting question set", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	if questionSet == nil {
		return nil, errdefs.ErrQuestionSetNotFound
	}

	// if host chose to shuffle questions,
	// each time this method is called it will return
	// a different element positioning
	if questionSet.Shuffle {
		questionSet.ShuffleQuestions()
	}

	return questionSet, nil
}

func (qu *questions) BuildDataset(ctx context.Context, numQuestions int32) error {
	// New grouped logger
	logger := qu.logger.WithGroup("build_dataset").With("numQuestions", numQuestions)

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "BuildDataset called")

	span := qu.telemetry.Record(ctx, "BuildDataset", map[string]string{
		"numQuestions": strconv.Itoa(int(numQuestions)),
	})
	defer span.End()

	logger.LogAttrs(ctx, slog.LevelDebug, "Building dataset")

	if qu.repository.IsSeeded(ctx) {
		logger.LogAttrs(ctx, slog.LevelDebug, "Question dataset already exists")
		return nil
	}

	logger.LogAttrs(ctx, slog.LevelInfo, "Seeding data")
	var hardQuestions, mediumQuestions, easyQuestions []model.Question

	perCategory := int(numQuestions / 3)

	hardQuestions = make([]model.Question, 0, perCategory)
	mediumQuestions = make([]model.Question, 0, perCategory)
	easyQuestions = make([]model.Question, 0, perCategory)

	for i := 0; i < perCategory; i++ {
		easyQuestions = append(easyQuestions, newQuestion(0.2))
		mediumQuestions = append(mediumQuestions, newQuestion(0.5))
		hardQuestions = append(hardQuestions, newQuestion(1))
	}

	logger.Info("Generated questions", "easy", len(easyQuestions), "medium", len(mediumQuestions), "hard", len(hardQuestions))

	logger.LogAttrs(ctx, slog.LevelDebug, "Saving questions to database")
	err := qu.repository.SaveLevelSet(ctx, model.Easy, easyQuestions)
	if err != nil {
		return errdefs.ErrDatabaseFailed
	}

	logger.LogAttrs(ctx, slog.LevelDebug, "Saved easy questions")
	err = qu.repository.SaveLevelSet(ctx, model.Medium, mediumQuestions)
	if err != nil {
		return errdefs.ErrDatabaseFailed
	}

	logger.LogAttrs(ctx, slog.LevelDebug, "Saved medium questions")
	err = qu.repository.SaveLevelSet(ctx, model.Hard, hardQuestions)
	if err != nil {
		return errdefs.ErrDatabaseFailed
	}

	logger.LogAttrs(ctx, slog.LevelDebug, "Saved hard questions")
	err = qu.repository.MarkAsSeeded(ctx)
	if err != nil {
		logger.Error("Could not mark database as seeded", "error", err)
		return errdefs.ErrDatabaseFailed
	}

	logger.LogAttrs(ctx, slog.LevelInfo, "Marked database as seeded successfully")

	qu.telemetry.IncrementCounter(ctx, constants.QuestionsCreatedCounterKey, numQuestions)

	return nil
}

func newQuestion(difficulty float32) model.Question {
	if rand.Intn(2) == 0 {
		return questionTypes.NewBinaryToDecimal(difficulty)
	}
	return questionTypes.NewDecimalToBinary(difficulty)
}
