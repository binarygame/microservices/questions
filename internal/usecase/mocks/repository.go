package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

type MockQuestionsRepository struct {
	mock.Mock
}

func (m *MockQuestionsRepository) GetRandomQuestions(ctx context.Context, level questionsModel.QuestionSetLevel, count int64) ([]questionsModel.Question, error) {
	args := m.Called(ctx, level, count)
	if args.Get(0) != nil {
		return args.Get(0).([]questionsModel.Question), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockQuestionsRepository) SaveQuestionSet(ctx context.Context, questionSet questionsModel.QuestionSet) error {
	args := m.Called(ctx, questionSet)
	return args.Error(0)
}

func (m *MockQuestionsRepository) GetQuestionSet(ctx context.Context, questionSetID string) (*questionsModel.QuestionSet, error) {
	args := m.Called(ctx, questionSetID)
	if args.Get(0) != nil {
		return args.Get(0).(*questionsModel.QuestionSet), args.Error(1)
	}

	return nil, args.Error(1)
}

func (m *MockQuestionsRepository) SaveLevelSet(ctx context.Context, level questionsModel.QuestionSetLevel, questions []questionsModel.Question) error {
	args := m.Called(ctx, level, questions)
	return args.Error(0)
}

func (m *MockQuestionsRepository) IsSeeded(ctx context.Context) bool {
	args := m.Called(ctx)
	return args.Bool(0)
}

func (m *MockQuestionsRepository) MarkAsSeeded(ctx context.Context) error {
	args := m.Called(ctx)
	return args.Error(0)
}
