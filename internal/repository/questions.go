package repository

import (
	"context"
	"log/slog"
	"strconv"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
	"gitlab.com/binarygame/microservices/questions/pkg/constants"
	"gitlab.com/binarygame/microservices/questions/pkg/errdefs"
	questionTypes "gitlab.com/binarygame/microservices/questions/pkg/models/question_types"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

type QuestionsRepository interface {
	GetRandomQuestions(ctx context.Context, level questionsModel.QuestionSetLevel, count int64) ([]questionsModel.Question, error)
	SaveQuestionSet(ctx context.Context, questionSet questionsModel.QuestionSet) error
	GetQuestionSet(ctx context.Context, questionSetID string) (*questionsModel.QuestionSet, error)
	SaveLevelSet(ctx context.Context, level questionsModel.QuestionSetLevel, questions []questionsModel.Question) error
	IsSeeded(ctx context.Context) bool
	MarkAsSeeded(ctx context.Context) error
}

type questionsRepository struct {
	db     *redis.Client
	logger *slog.Logger
}

func New(address string, logger *slog.Logger) QuestionsRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// Enable tracing instrumentation.
	if err := redisotel.InstrumentTracing(client); err != nil {
		panic(err)
	}

	// Enable metrics instrumentation.
	if err := redisotel.InstrumentMetrics(client); err != nil {
		panic(err)
	}

	return &questionsRepository{
		db:     client,
		logger: logger,
	}
}

func (qr *questionsRepository) GetRandomQuestions(ctx context.Context, level questionsModel.QuestionSetLevel, count int64) ([]questionsModel.Question, error) {
	qlKey := qr.buildQuestionsLevelKey(level)

	qr.logger.Debug("Getting Random Questions", "levelKey", qlKey, "count", count)

	ids, err := qr.db.SRandMemberN(ctx, qlKey, count).Result()
	if err != nil {
		qr.logger.Error("Error getting random questions from Redis Set", "error", err)
		return nil, err
	}

	questions := make([]questionsModel.Question, len(ids))

	for i, id := range ids {
		q, err := qr.db.HGetAll(ctx, buildQuestionsKey(id)).Result()
		if err != nil {
			qr.logger.Error("Error retrieving question data", "error", err)
			return nil, err
		}

		var difficulty float32
		var questionType questionsModel.QuestionType

		v, exists := q["difficulty"]
		if !exists {
			qr.logger.Error("Question difficulty not found")
			return nil, errdefs.ErrQuestionDifficultyNotFound
		}
		d, err := strconv.ParseFloat(v, 32)
		if err != nil {
			qr.logger.Error("Error parsing question difficulty", "error", err)
			return nil, err
		}
		difficulty = float32(d)

		v, exists = q["type"]
		if !exists {
			qr.logger.Error("Question type not found")
			return nil, errdefs.ErrQuestionTypeNotFound
		}
		qt, err := strconv.ParseInt(v, 10, 8)
		if err != nil {
			qr.logger.Error("Error parsing question type", "error", err)
			return nil, err
		}
		questionType = questionsModel.QuestionType(uint8(qt))

		switch questionType {
		case questionsModel.BinaryToDecimal:
			questions[i] = questionTypes.NewBinaryToDecimalWithId(id, difficulty)
		case questionsModel.DecimalToBinary:
			questions[i] = questionTypes.NewDecimalToBinaryWithId(id, difficulty)
		default:
			qr.logger.Error("Unknown question type", "questionType", int(questionType))
			return nil, errdefs.ErrQuestionTypeUnknown
		}
	}

	return questions, nil
}

func (qr *questionsRepository) SaveQuestionSet(ctx context.Context, questionSet questionsModel.QuestionSet) error {
	qSetKey := buildQuestionSetKey(questionSet.ID)

	qr.logger.Debug("Saving Question Set", "questionSetID", questionSet.ID)

	pipe := qr.db.Pipeline() // Create a pipeline

	for _, q := range questionSet.Questions {
		pipe.RPush(ctx, qSetKey, q.GetID()) // Add to the pipeline
	}

	// Add the metadata to the pipeline
	pipe.HSet(ctx, qSetKey+":meta",
		"id", questionSet.ID,
		"level", qr.questionSetLevelToString(questionSet.Level),
		"shuffle", questionSet.Shuffle,
	)

	// Set expiration in the pipeline
	pipe.Expire(ctx, qSetKey, constants.ExpireDuration)

	// Execute the pipeline
	_, err := pipe.Exec(ctx)
	if err != nil {
		qr.logger.Error("Error executing pipeline", "error", err)
		return err
	}

	return nil
}

func (qr *questionsRepository) GetQuestionSet(ctx context.Context, questionSetID string) (*questionsModel.QuestionSet, error) {
	qSetKey := buildQuestionSetKey(questionSetID)

	qr.logger.Debug("Retrieving Question Set", "questionSetID", questionSetID)

	ids, err := qr.db.LRange(ctx, qSetKey, 0, -1).Result()
	if err != nil {
		qr.logger.Error("Error retrieving question IDs from Redis List", "error", err, "key", qSetKey)
		return nil, err
	}

	if len(ids) == 0 {
		qr.logger.Error("No questions found in Question Set", "questionSetID", questionSetID)
		return nil, nil
	}

	meta, err := qr.db.HGetAll(ctx, qSetKey+":meta").Result()
	if err != nil {
		qr.logger.Error("Error retrieving Question Set metadata from Redis", "error", err, "key", qSetKey+":meta")
		return nil, err
	}

	questions := make([]questionsModel.Question, len(ids))
	for i, id := range ids {
		qr.logger.Debug("Getting data for question", "id", id, "key", buildQuestionsKey(id))
		qData, err := qr.db.HGetAll(ctx, buildQuestionsKey(id)).Result()
		if err != nil {
			qr.logger.Error("Error retrieving question data from Redis", "error", err, "key", buildQuestionsKey(id))
			return nil, err
		}

		qr.logger.Debug("Data from redis", "difficulty", qData["difficulty"], "type", qData["type"], "query", qData["query"], "answer", qData["answer"])

		difficulty, err := strconv.ParseFloat(qData["difficulty"], 32)
		if err != nil {
			qr.logger.Error("Error parsing question difficulty", "error", err)
			return nil, err
		}

		qt, err := strconv.ParseInt(qData["type"], 10, 8)
		if err != nil {
			qr.logger.Error("Error parsing question type", "error", err)
			return nil, err
		}

		questions[i] = &questionsModel.BaseQuestion{
			QuestionID: id,
			Difficulty: float32(difficulty),
			Type:       questionsModel.QuestionType(uint8(qt)),
			Query:      qData["query"],
			Answer:     qData["answer"],
		}
	}

	level := qr.stringToQuestionSetLevel(meta["level"])
	shuffle := meta["shuffle"] == "1"

	questionSet := &questionsModel.QuestionSet{
		ID:        questionSetID,
		Level:     level,
		Questions: questions,
		Shuffle:   shuffle,
	}

	return questionSet, nil
}

func (qr *questionsRepository) SaveLevelSet(ctx context.Context, level questionsModel.QuestionSetLevel, questions []questionsModel.Question) error {
	qlKey := qr.buildQuestionsLevelKey(level)

	qr.logger.Debug("Saving Level Set", "levelKey", qlKey)

	pipe := qr.db.Pipeline() // sends all commands in a single batch

	for _, question := range questions {
		qKey := buildQuestionsKey(question.GetID())

		pipe.HSet(ctx, qKey,
			"difficulty", question.GetDifficulty(),
			"query", question.GetQuery(),
			"answer", question.GetAnswer(),
			"type", strconv.Itoa(int(question.GetType())),
		)

		pipe.SAdd(ctx, qlKey, question.GetID())
	}

	_, err := pipe.Exec(ctx)
	if err != nil {
		qr.logger.Error("Error executing pipeline to save Level Set", "error", err)
		return err
	}

	return nil
}

func (qr *questionsRepository) IsSeeded(ctx context.Context) bool {
	qr.logger.Debug("Checking if questions database is seeded")

	result, err := qr.db.Get(ctx, seedingStatusKey).Result()
	if err == redis.Nil {
		// Key does not exist
		return false
	} else if err != nil {
		qr.logger.Error("Error checking if questions are seeded", "error", err)
		return false
	}

	return result == "seeded"
}

func (qr *questionsRepository) MarkAsSeeded(ctx context.Context) error {
	qr.logger.Debug("Marking questions database as seeded")

	err := qr.db.Set(ctx, seedingStatusKey, "seeded", 0).Err()
	if err != nil {
		qr.logger.Error("Error marking questions database as seeded", "error", err)
		return err
	}

	return nil
}
