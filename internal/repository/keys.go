package repository

import (
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

const (
	questionsRedisKey = "questions"
	questionSetsKey   = "questionSets"
	seedingStatusKey  = "questions:seeded"
)

func buildQuestionsKey(questionID string) string {
	return questionsRedisKey + ":" + questionID
}

func buildQuestionSetKey(questionSetID string) string {
	return questionSetsKey + ":" + questionSetID
}

func (qr *questionsRepository) buildQuestionsLevelKey(level questionsModel.QuestionSetLevel) string {
	levelKey := ""
	switch level {
	case questionsModel.Easy:
		levelKey = questionsRedisKey + ":easy"
	case questionsModel.Medium:
		levelKey = questionsRedisKey + ":medium"
	case questionsModel.Hard:
		levelKey = questionsRedisKey + ":hard"
	default:
		qr.logger.Error("Invalid QuestionSetLevel", "level", level)
	}
	qr.logger.Debug("Built Questions Level Key", "level", level, "levelKey", levelKey)
	return levelKey
}

// questionSetLevelToString converts QuestionSetLevel to its string representation.
func (qr *questionsRepository) questionSetLevelToString(level questionsModel.QuestionSetLevel) string {
	strLevel := ""
	switch level {
	case questionsModel.Easy:
		strLevel = "easy"
	case questionsModel.Medium:
		strLevel = "medium"
	case questionsModel.Hard:
		strLevel = "hard"
	default:
		qr.logger.Error("Invalid QuestionSetLevel", "level", level)
	}
	qr.logger.Debug("Converted QuestionSetLevel to string", "level", level, "strLevel", strLevel)
	return strLevel
}

// stringToQuestionSetLevel converts a string representation to QuestionSetLevel.
func (qr *questionsRepository) stringToQuestionSetLevel(level string) questionsModel.QuestionSetLevel {
	var setLevel questionsModel.QuestionSetLevel
	switch level {
	case "easy":
		setLevel = questionsModel.Easy
	case "medium":
		setLevel = questionsModel.Medium
	case "hard":
		setLevel = questionsModel.Hard
	default:
		qr.logger.Error("Invalid string level for QuestionSetLevel", "level", level)
		setLevel = questionsModel.QuestionSetLevel(0) // Default to 0 (invalid level)
	}
	qr.logger.Debug("Converted string to QuestionSetLevel", "strLevel", level, "setLevel", setLevel)
	return setLevel
}
