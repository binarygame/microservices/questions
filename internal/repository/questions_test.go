package repository_test

import (
	"bytes"
	"context"
	"log/slog"
	"strconv"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/questions/internal/repository"
	questions "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

const questionsRedisKey = "questions"

var (
	logBuffer bytes.Buffer
	logger    *slog.Logger = slog.New(slog.NewTextHandler(&logBuffer, nil))
)

func setupTestRedis(t *testing.T) (*redis.Client, *miniredis.Miniredis) {
	s := miniredis.RunT(t)

	client := redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})

	return client, s
}

func TestGetRandomQuestions(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		count := int64(5)
		mock.SAdd("questions:easy", "q1", "q2", "q3", "q4", "q5")
		mock.HSet("questions:q1", "difficulty", "1.0", "type", "1")
		mock.HSet("questions:q2", "difficulty", "1.0", "type", "1")
		mock.HSet("questions:q3", "difficulty", "1.0", "type", "1")
		mock.HSet("questions:q4", "difficulty", "1.0", "type", "1")
		mock.HSet("questions:q5", "difficulty", "1.0", "type", "1")

		questions, err := repo.GetRandomQuestions(ctx, level, count)
		assert.NoError(t, err)
		assert.Len(t, questions, 5)
	})

	t.Run("empty database", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		count := int64(5)

		questions, err := repo.GetRandomQuestions(ctx, level, count)
		assert.NoError(t, err)
		assert.Len(t, questions, 0)
	})

	t.Run("database fail", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		count := int64(5)
		mock.Close() // Simulate database error

		_, err := repo.GetRandomQuestions(ctx, level, count)
		assert.Error(t, err)
	})

	t.Run("randomness verification", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		count := int64(5)

		for i := 0; i < 1000; i++ {
			mock.HSet("questions:q"+strconv.Itoa(i), "difficulty", "1.0", "type", "1")
			mock.SAdd("questions:easy", "q"+strconv.Itoa(i))
		}

		s1, err1 := repo.GetRandomQuestions(ctx, level, count)
		s2, err2 := repo.GetRandomQuestions(ctx, level, count)

		require.NoError(t, err1)
		require.NoError(t, err2)

		assert.NotEqual(t, s1, s2)
	})
}

func TestSaveQuestionSet(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		questionSet := questions.QuestionSet{
			ID:    "qs1",
			Level: questions.Easy,
			Questions: []questions.Question{
				&questions.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: questions.BinaryToDecimal},
			},
		}

		err := repo.SaveQuestionSet(ctx, questionSet)
		assert.NoError(t, err)

		ids, err := client.LRange(ctx, "questionSets:qs1", 0, 1).Result()
		assert.NoError(t, err)
		assert.Len(t, ids, len(questionSet.Questions))
	})

	t.Run("database fail", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		questionSet := questions.QuestionSet{
			ID:    "qs1",
			Level: questions.Easy,
			Questions: []questions.Question{
				&questions.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: questions.BinaryToDecimal},
			},
		}
		mock.Close() // Simulate database error

		err := repo.SaveQuestionSet(ctx, questionSet)
		assert.Error(t, err)
	})
}

func TestGetQuestionSet(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		mock.RPush("questionSets:qs1", "q1")
		mock.HSet("questions:q1", "difficulty", "1.0", "type", "1", "query", "10", "answer", "2")
		mock.HSet("questionSets:qs1:meta", "id", "qs1", "level", "easy")

		questionSet, err := repo.GetQuestionSet(ctx, "qs1")
		assert.NoError(t, err)
		assert.NotEqual(t, questionSet.Level, questions.Unknown)
		assert.NotNil(t, questionSet)
	})

	t.Run("database fail", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		mock.Close() // Simulate database error

		questionSet, err := repo.GetQuestionSet(ctx, "qs1")
		assert.Error(t, err)
		assert.Nil(t, questionSet)
	})

	t.Run("invalid question", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		mock.RPush("questionSets:qs1", "q1")
		mock.HSet("questions:q1", "difficulty", "1.0", "type", "1", "query", "10", "answer", "2")
		mock.HSet("questionSets:meta", "level", "easy")

		// Add invalid question
		mock.RPush("questionSets:qs1", "q2")

		questionSet, err := repo.GetQuestionSet(ctx, "qs1")
		assert.Error(t, err)
		assert.Nil(t, questionSet)
	})

	t.Run("set not found", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		questionSet, err := repo.GetQuestionSet(ctx, "qs1")
		assert.NoError(t, err)
		assert.Nil(t, questionSet)
	})
}

func TestSaveLevelSet(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		q := questions.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: questions.BinaryToDecimal}
		err := repo.SaveLevelSet(ctx, level, []questions.Question{&q})

		assert.NoError(t, err)
	})

	t.Run("empty set", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		err := repo.SaveLevelSet(ctx, level, []questions.Question{})

		assert.NoError(t, err)
	})

	t.Run("database fail", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		level := questions.Easy
		q1 := questions.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: questions.BinaryToDecimal}
		q2 := questions.BaseQuestion{QuestionID: "q2", Difficulty: 1.0, Type: questions.BinaryToDecimal}
		q3 := questions.BaseQuestion{QuestionID: "q3", Difficulty: 1.0, Type: questions.BinaryToDecimal} // Invalid question
		mock.Close()                                                                                     // Simulate database error

		err := repo.SaveLevelSet(ctx, level, []questions.Question{&q1, &q2, &q3})
		assert.Error(t, err)
	})
}

func TestIsSeeded(t *testing.T) {
	t.Run("seeded", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		mock.Set("questions:seeded", "seeded")

		seeded := repo.IsSeeded(ctx)
		assert.True(t, seeded)
	})

	t.Run("unseeded", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		seeded := repo.IsSeeded(ctx)
		assert.False(t, seeded)
	})

	t.Run("database fail", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		mock.Close() // Simulate database error

		seeded := repo.IsSeeded(ctx)
		assert.False(t, seeded)
	})
}

func TestMarkAsSeeded(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		err := repo.MarkAsSeeded(ctx)
		assert.NoError(t, err)

		result, err := client.Get(ctx, "questions:seeded").Result()
		assert.NoError(t, err)
		assert.Equal(t, "seeded", result)
	})

	t.Run("database fail", func(t *testing.T) {
		ctx := context.Background()
		client, mock := setupTestRedis(t)
		defer mock.Close()
		repo := repository.New(client.Options().Addr, logger)

		mock.Close() // Simulate database error

		err := repo.MarkAsSeeded(ctx)
		assert.Error(t, err)
	})
}
