package server

import (
	"context"
	"log/slog"
	"net/http"
	"strconv"

	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"

	"connectrpc.com/connect"
	"connectrpc.com/grpcreflect"
	"connectrpc.com/otelconnect"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/questions/internal/repository"
	"gitlab.com/binarygame/microservices/questions/internal/usecase"
	"gitlab.com/binarygame/microservices/questions/pkg/constants"
	"gitlab.com/binarygame/microservices/questions/pkg/errdefs"
	questionsv1 "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1"
	"gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1/questionsv1connect"
	model "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

type QuestionsServer struct {
	questionsv1connect.UnimplementedQuestionsServiceHandler
	usecase usecase.Questions // Use the Questions interface here
	logger  *slog.Logger
}

func newQuestionsServer(usecase usecase.Questions, logger *slog.Logger) *QuestionsServer {
	return &QuestionsServer{
		usecase: usecase,
		logger:  logger,
	}
}

func Start(ip string, port int, repository repository.QuestionsRepository, logger *slog.Logger, ts telemetry.TelemetryService) {
	addr := ip + ":" + strconv.Itoa(port)

	ts.NewCounter(constants.SetsCreatedCounterKey, "Number of generated sets", telemetry.IntCounterType)
	ts.NewCounter(constants.QuestionsCreatedCounterKey, "Number of generated questions", telemetry.IntCounterType)

	// Initialize the use case with the logger
	usecase := usecase.New(logger, repository, ts)
	otelInterceptor, err := otelconnect.NewInterceptor(otelconnect.WithTrustRemote())
	if err != nil {
		logger.Error("An error occurred in otelconnect.NewInterceptor", "error", err)
	}

	server := newQuestionsServer(usecase, logger)
	mux := http.NewServeMux()
	path, handler := questionsv1connect.NewQuestionsServiceHandler(
		server,
		connect.WithInterceptors(otelInterceptor),
	)

	// Handle GRPC reflection
	reflector := grpcreflect.NewStaticReflector(
		questionsv1connect.QuestionsServiceName,
	)

	mux.Handle(path, handler)
	mux.Handle(grpcreflect.NewHandlerV1(reflector))
	mux.Handle(grpcreflect.NewHandlerV1Alpha(reflector))

	err = usecase.BuildDataset(context.Background(), constants.NumGeneratedQuestions)
	if err != nil {
		logger.Error("Failed building questions dataset", "error", err.Error())
	}

	logger.Info("Starting connect server for binarygame-questions", "addr", addr)

	err = http.ListenAndServe(
		addr,
		h2c.NewHandler(mux, &http2.Server{}),
	)
	if err != nil {
		logger.Error("An error occurred in http.ListenAndServe:", "error", err)
	}
}

func (qs *QuestionsServer) GenerateQuestionSet(ctx context.Context, r *connect.Request[questionsv1.GenerateQuestionSetRequest]) (*connect.Response[questionsv1.GenerateQuestionSetResponse], error) {
	qs.logger.Debug("Received GenerateQuestionSet request", "QuestionCount", r.Msg.QuestionCount, "Difficulty", int32(r.Msg.Difficulty))

	var level model.QuestionSetLevel
	switch r.Msg.Difficulty {
	case questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY:
		level = model.Easy
	case questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_MEDIUM:
		level = model.Medium
	case questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_HARD:
		level = model.Hard
	default:
		return nil, connect.NewError(connect.CodeInvalidArgument, errdefs.ErrQuestionDifficultyOutOfRange)
	}

	questionSet, err := qs.usecase.GenerateQuestionSet(ctx, r.Msg.QuestionCount, level, r.Msg.Shuffle)
	if err != nil {
		var connectErr *connect.Error

		switch err {
		case errdefs.ErrQuestionCountOutOfRange:
			connectErr = connect.NewError(connect.CodeOutOfRange, err)
		case errdefs.ErrNoQuestionsFound:
			connectErr = connect.NewError(connect.CodeInternal, err)
		case errdefs.ErrDatabaseFailed:
			connectErr = connect.NewError(connect.CodeInternal, err)
		default:
			connectErr = connect.NewError(connect.CodeUnknown, err)
		}

		qs.logger.Debug("Returning error response", "error", connectErr)
		return nil, connectErr
	}

	// Map question set to the response format
	responseQuestions := make([]*questionsv1.Question, len(questionSet.Questions))
	for i, question := range questionSet.Questions {
		var questionType questionsv1.QuestionType
		switch question.GetType() {
		case model.BinaryToDecimal:
			questionType = questionsv1.QuestionType_QUESTION_TYPE_BINARY_TO_DECIMAL
		case model.DecimalToBinary:
			questionType = questionsv1.QuestionType_QUESTION_TYPE_DECIMAL_TO_BINARY
		default:
			questionType = questionsv1.QuestionType_QUESTION_TYPE_UNSPECIFIED
		}
		responseQuestions[i] = &questionsv1.Question{
			Id:     question.GetID(),
			Type:   questionType,
			Query:  question.GetQuery(),
			Answer: question.GetAnswer(),
		}
	}

	return connect.NewResponse(&questionsv1.GenerateQuestionSetResponse{
		QuestionSet: &questionsv1.QuestionSet{
			Id:        questionSet.ID,
			Level:     r.Msg.Difficulty,
			Questions: responseQuestions,
		},
	}), nil
}

func (qs *QuestionsServer) GetQuestionSet(ctx context.Context, r *connect.Request[questionsv1.GetQuestionSetRequest]) (*connect.Response[questionsv1.GetQuestionSetResponse], error) {
	qs.logger.Debug("Received GetQuestionSet request", "QuestionSetID", r.Msg.QuestionSetId)

	questionSet, err := qs.usecase.GetQuestionSet(ctx, r.Msg.QuestionSetId)
	if err != nil {
		var connectErr *connect.Error

		switch err {
		case errdefs.ErrQuestionSetIDEmpty:
			connectErr = connect.NewError(connect.CodeInvalidArgument, err)
		case errdefs.ErrQuestionSetNotFound:
			connectErr = connect.NewError(connect.CodeNotFound, err)
		case errdefs.ErrDatabaseFailed:
			connectErr = connect.NewError(connect.CodeInternal, err)
		default:
			connectErr = connect.NewError(connect.CodeUnknown, err)
		}

		qs.logger.Debug("Returning error response", "error", connectErr)
		return nil, connectErr
	}

	// Map question set to the response format
	responseQuestions := make([]*questionsv1.Question, len(questionSet.Questions))
	for i, question := range questionSet.Questions {
		var questionType questionsv1.QuestionType
		switch question.GetType() {
		case model.BinaryToDecimal:
			questionType = questionsv1.QuestionType_QUESTION_TYPE_BINARY_TO_DECIMAL
		case model.DecimalToBinary:
			questionType = questionsv1.QuestionType_QUESTION_TYPE_DECIMAL_TO_BINARY
		default:
			questionType = questionsv1.QuestionType_QUESTION_TYPE_UNSPECIFIED
		}
		responseQuestions[i] = &questionsv1.Question{
			Id:     question.GetID(),
			Type:   questionType,
			Query:  question.GetQuery(),
			Answer: question.GetAnswer(),
		}
	}

	return connect.NewResponse(&questionsv1.GetQuestionSetResponse{
		QuestionSet: &questionsv1.QuestionSet{
			Id:        questionSet.ID,
			Level:     questionsv1.DifficultyLevel(questionSet.Level),
			Questions: responseQuestions,
		},
	}), nil
}
