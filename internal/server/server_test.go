package server

import (
	"bytes"
	"context"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"

	"connectrpc.com/connect"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/binarygame/microservices/questions/internal/server/mocks"
	questionsv1 "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1"
	"gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1/questionsv1connect"

	model "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

func initTestServer() (*httptest.Server, *mocks.MockQuestionsUsecase) {
	var logOutput bytes.Buffer
	logger := slog.New(slog.NewTextHandler(&logOutput, nil))
	mockUsecase := new(mocks.MockQuestionsUsecase)
	service := newQuestionsServer(mockUsecase, logger)

	mux := http.NewServeMux()
	mux.Handle(questionsv1connect.NewQuestionsServiceHandler(
		service,
	))

	testServer := httptest.NewUnstartedServer(mux)
	testServer.EnableHTTP2 = true
	return testServer, mockUsecase
}

func initClients(testServer *httptest.Server) []questionsv1connect.QuestionsServiceClient {
	connectClient := questionsv1connect.NewQuestionsServiceClient(
		testServer.Client(),
		testServer.URL,
	)
	connectGrpcClient := questionsv1connect.NewQuestionsServiceClient(
		testServer.Client(),
		testServer.URL,
		connect.WithGRPC(),
	)
	return []questionsv1connect.QuestionsServiceClient{connectClient, connectGrpcClient}
}

func TestGenerateQuestionSetRequest(t *testing.T) {
	t.Parallel()

	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	qArr := []model.Question{
		&model.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: model.BinaryToDecimal},
		&model.BaseQuestion{QuestionID: "q2", Difficulty: 1.0, Type: model.BinaryToDecimal},
		&model.BaseQuestion{QuestionID: "q3", Difficulty: 1.0, Type: model.DecimalToBinary},
		&model.BaseQuestion{QuestionID: "q4", Difficulty: 1.0, Type: model.DecimalToBinary},
		&model.BaseQuestion{QuestionID: "q5", Difficulty: 1.0, Type: model.DecimalToBinary},
	}
	qSet := &model.QuestionSet{ID: "qs1", Level: model.Easy, Questions: qArr}

	t.Run("success", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("GenerateQuestionSet", mock.Anything, mock.AnythingOfType("int32"), mock.Anything).Return(qSet, nil).Once()

			result, err := client.GenerateQuestionSet(
				context.Background(),
				connect.NewRequest(&questionsv1.GenerateQuestionSetRequest{
					QuestionCount: 5,
					Difficulty:    questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY,
				}))
			require.Nil(t, err)
			assert.Len(t, result.Msg.QuestionSet.Questions, 5)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("fail", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("GenerateQuestionSet", mock.Anything, mock.AnythingOfType("int32"), mock.Anything).Return(nil, assert.AnError).Once()

			_, err := client.GenerateQuestionSet(
				context.Background(),
				connect.NewRequest(&questionsv1.GenerateQuestionSetRequest{
					QuestionCount: 5,
					Difficulty:    questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY,
				}))
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestGetQuestionSetRequest(t *testing.T) {
	t.Parallel()

	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	qArr := []model.Question{
		&model.BaseQuestion{QuestionID: "q1", Difficulty: 1.0, Type: model.BinaryToDecimal},
		&model.BaseQuestion{QuestionID: "q2", Difficulty: 1.0, Type: model.BinaryToDecimal},
		&model.BaseQuestion{QuestionID: "q3", Difficulty: 1.0, Type: model.DecimalToBinary},
		&model.BaseQuestion{QuestionID: "q4", Difficulty: 1.0, Type: model.DecimalToBinary},
		&model.BaseQuestion{QuestionID: "q5", Difficulty: 1.0, Type: model.DecimalToBinary},
	}
	qSet := &model.QuestionSet{ID: "qs1", Level: model.Easy, Questions: qArr}

	t.Run("success", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("GetQuestionSet", mock.Anything, mock.AnythingOfType("string")).Return(qSet, nil).Once()

			result, err := client.GetQuestionSet(
				context.Background(),
				connect.NewRequest(&questionsv1.GetQuestionSetRequest{
					QuestionSetId: "qs1",
				}))
			require.Nil(t, err)
			assert.Equal(t, qSet.ID, result.Msg.QuestionSet.Id)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("fail", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("GetQuestionSet", mock.Anything, mock.AnythingOfType("string")).Return(nil, assert.AnError).Once()

			_, err := client.GetQuestionSet(
				context.Background(),
				connect.NewRequest(&questionsv1.GetQuestionSetRequest{
					QuestionSetId: "qs1",
				}))
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})
}
