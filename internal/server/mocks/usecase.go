package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	model "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

type MockQuestionsUsecase struct {
	mock.Mock
}

func (m *MockQuestionsUsecase) GenerateQuestionSet(ctx context.Context, questionCount int32, level model.QuestionSetLevel) (*model.QuestionSet, error) {
	args := m.Called(ctx, questionCount, level)
	if args.Get(0) != nil {
		return args.Get(0).(*model.QuestionSet), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockQuestionsUsecase) GetQuestionSet(ctx context.Context, questionSetID string) (*model.QuestionSet, error) {
	args := m.Called(ctx, questionSetID)
	if args.Get(0) != nil {
		return args.Get(0).(*model.QuestionSet), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockQuestionsUsecase) BuildDataset(ctx context.Context, numQuestions int32) error {
	args := m.Called(ctx, numQuestions)
	return args.Error(0)
}
