package main

import (
	"context"
	"errors"
	"log/slog"

	"github.com/joho/godotenv"
	"gitlab.com/binarygame/microservices/questions/internal/repository"
	service "gitlab.com/binarygame/microservices/questions/internal/server"
	"gitlab.com/binarygame/microservices/questions/pkg/constants"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		slog.Info("Error loading .env file")
	}

	serviceHost := bingameutils.GetServiceHost()
	servicePort := bingameutils.GetServicePort()

	logger := bingameutils.GetLogger()
	slog.SetDefault(logger)

	// Set up OpenTelemetry.
	ts, otelShutdown, err := telemetry.New(context.Background(), constants.NamespaceKey)
	if err != nil {
		logger.Error("failed to set up OpenTelemetry SDK", "error", err.Error())
	}
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()

	repository := repository.New(bingameutils.GetValkeyAddress(), logger)
	service.Start(serviceHost, servicePort, repository, logger, ts)
}
