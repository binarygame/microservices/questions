package models

import (
	"math/rand"
	"strconv"

	"gitlab.com/binarygame/microservices/questions/pkg/constants"
	questions "gitlab.com/binarygame/microservices/questions/pkg/models/questions"

	"github.com/google/uuid"
)

type DecimalToBinary struct {
	questions.BaseQuestion
}

func NewDecimalToBinary(difficulty float32) questions.Question {
	dtb := DecimalToBinary{
		BaseQuestion: questions.BaseQuestion{
			QuestionID: uuid.New().String(),
			Difficulty: difficulty,
			Type:       questions.DecimalToBinary,
		},
	}

	query, answer := dtb.GenerateQuestion()

	dtb.Query = query
	dtb.Answer = answer

	return &dtb
}

func NewDecimalToBinaryWithId(id string, difficulty float32) questions.Question {
	dtb := DecimalToBinary{
		BaseQuestion: questions.BaseQuestion{
			QuestionID: id,
			Difficulty: difficulty,
			Type:       questions.DecimalToBinary,
		},
	}

	query, answer := dtb.GenerateQuestion()

	dtb.Query = query
	dtb.Answer = answer

	return &dtb
}

// generateBinaryToDecimalQuestion generates a binary-to-decimal question based on the given difficulty level and seed.
func (dtb *DecimalToBinary) GenerateQuestion() (string, string) {
	rand := rand.New(rand.NewSource(int64(questions.SeedHash(dtb.GetID())))) // Seed the random number generator based on the question id.

	// Determine the maximum decimal number based on difficulty.
	maxDecimal := int(dtb.GetDifficulty() * constants.MaxDecimalNumber) // Scale the difficulty to determine the maximum decimal number.
	if maxDecimal < 1 {
		maxDecimal = 1
	}

	// Generate a random decimal number within the specified range.
	decimalNumber := rand.Intn(maxDecimal)

	// Convert the decimal number to binary.
	binaryNumberString := strconv.FormatInt(int64(decimalNumber), 2)
	decimalNumberString := strconv.Itoa(decimalNumber)

	return decimalNumberString, binaryNumberString
}
