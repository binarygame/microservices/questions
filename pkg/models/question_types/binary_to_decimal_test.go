package models_test

import (
	"strconv"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	models "gitlab.com/binarygame/microservices/questions/pkg/models/question_types"
	questions "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

func TestNewBinaryToDecimal(t *testing.T) {
	difficulty := float32(1.0)
	question := models.NewBinaryToDecimal(difficulty)

	assert.NotNil(t, question)
	assert.Equal(t, difficulty, question.GetDifficulty())
	assert.Equal(t, questions.BinaryToDecimal, question.GetType())
	assert.NotEmpty(t, question.GetID())
	assert.NotEmpty(t, question.GetQuery())
	assert.NotEmpty(t, question.GetAnswer())
}

func TestNewBinaryToDecimalWithId(t *testing.T) {
	id := uuid.New().String()
	difficulty := float32(1.0)
	question := models.NewBinaryToDecimalWithId(id, difficulty)

	assert.NotNil(t, question)
	assert.Equal(t, id, question.GetID())
	assert.Equal(t, difficulty, question.GetDifficulty())
	assert.Equal(t, questions.BinaryToDecimal, question.GetType())
	assert.NotEmpty(t, question.GetQuery())
	assert.NotEmpty(t, question.GetAnswer())
}

func TestGenerateQuestionBTD(t *testing.T) {
	difficulty := float32(1.0)
	id := uuid.New().String()
	question := models.NewBinaryToDecimalWithId(id, difficulty).(*models.BinaryToDecimal)

	query, answer := question.GenerateQuestion()

	// Convert answer to an integer to check correctness
	decimalValue, err := strconv.Atoi(answer)
	assert.NoError(t, err)

	binaryValue, err := strconv.ParseInt(query, 2, 64)
	assert.NoError(t, err)

	assert.Equal(t, binaryValue, int64(decimalValue))
}
