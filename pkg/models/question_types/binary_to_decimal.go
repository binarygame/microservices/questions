package models

import (
	"math/rand"
	"strconv"

	"gitlab.com/binarygame/microservices/questions/pkg/constants"
	questions "gitlab.com/binarygame/microservices/questions/pkg/models/questions"

	"github.com/google/uuid"
)

type BinaryToDecimal struct {
	questions.BaseQuestion
}

func NewBinaryToDecimal(difficulty float32) questions.Question {
	btd := BinaryToDecimal{
		BaseQuestion: questions.BaseQuestion{
			QuestionID: uuid.New().String(),
			Difficulty: difficulty,
			Type:       questions.BinaryToDecimal,
		},
	}

	query, answer := btd.GenerateQuestion()

	btd.Query = query
	btd.Answer = answer

	return &btd
}

func NewBinaryToDecimalWithId(id string, difficulty float32) questions.Question {
	btd := BinaryToDecimal{
		BaseQuestion: questions.BaseQuestion{
			QuestionID: id,
			Difficulty: difficulty,
			Type:       questions.BinaryToDecimal,
		},
	}

	query, answer := btd.GenerateQuestion()

	btd.Query = query
	btd.Answer = answer

	return &btd
}

// generateBinaryToDecimalQuestion generates a binary-to-decimal question based on the given difficulty level and seed.
func (btd *BinaryToDecimal) GenerateQuestion() (string, string) {
	rand := rand.New(rand.NewSource((int64(questions.SeedHash(btd.GetID()))))) // Seed the random number generator based on the question id.

	// Determine the number of bits based on difficulty.
	maxBits := int(btd.GetDifficulty() * constants.MaxBitsNumber) // Scale the difficulty to determine the maximum number of bits.
	if maxBits < 1 {
		maxBits = 1
	}

	// Generate a random binary number with the specified number of bits.
	binaryNumberString := ""
	for i := 0; i < maxBits; i++ {
		bit := rand.Intn(2) // Generate random 0 or 1.
		binaryNumberString += strconv.Itoa(bit)
	}

	// Convert the binary number to decimal.
	decimalNumber, _ := strconv.ParseInt(binaryNumberString, 2, 64)
	decimalNumberString := strconv.Itoa(int(decimalNumber))

	return binaryNumberString, decimalNumberString
}
