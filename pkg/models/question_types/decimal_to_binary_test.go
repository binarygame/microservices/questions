package models_test

import (
	"strconv"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	model "gitlab.com/binarygame/microservices/questions/pkg/models/question_types"
	questions "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

func TestNewDecimalToBinary(t *testing.T) {
	difficulty := float32(1.0)
	question := model.NewDecimalToBinary(difficulty)

	assert.NotNil(t, question)
	assert.Equal(t, difficulty, question.GetDifficulty())
	assert.Equal(t, questions.DecimalToBinary, question.GetType())
	assert.NotEmpty(t, question.GetID())
	assert.NotEmpty(t, question.GetQuery())
	assert.NotEmpty(t, question.GetAnswer())
}

func TestNewDecimalToBinaryWithId(t *testing.T) {
	id := uuid.New().String()
	difficulty := float32(1.0)
	question := model.NewDecimalToBinaryWithId(id, difficulty)

	assert.NotNil(t, question)
	assert.Equal(t, id, question.GetID())
	assert.Equal(t, difficulty, question.GetDifficulty())
	assert.Equal(t, questions.DecimalToBinary, question.GetType())
	assert.NotEmpty(t, question.GetQuery())
	assert.NotEmpty(t, question.GetAnswer())
}

func TestGenerateQuestionDTB(t *testing.T) {
	difficulty := float32(1.0)
	id := uuid.New().String()
	question := model.NewDecimalToBinaryWithId(id, difficulty).(*model.DecimalToBinary)

	query, answer := question.GenerateQuestion()

	// Convert query to an integer to check correctness
	decimalValue, err := strconv.Atoi(query)
	assert.NoError(t, err)

	binaryValue, err := strconv.ParseInt(answer, 2, 64)
	assert.NoError(t, err)

	assert.Equal(t, int64(decimalValue), binaryValue)
}
