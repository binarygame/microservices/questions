package models

import (
	"math/rand"
	"strconv"

	"github.com/spaolacci/murmur3"
)

type QuestionType uint8

const (
	BinaryToDecimal QuestionType = iota
	DecimalToBinary
)

func (qt QuestionType) String() string {
	switch qt {
	case BinaryToDecimal:
		return "BinaryToDecimal"
	case DecimalToBinary:
		return "DecimalToBinary"
	default:
		return "Unknown:" + strconv.Itoa(int(qt))
	}
}

type QuestionSetLevel uint8

const (
	Unknown QuestionSetLevel = iota
	Easy
	Medium
	Hard
)

func (qsl QuestionSetLevel) String() string {
	switch qsl {
	case Unknown:
		return "Unknown"
	case Easy:
		return "Easy"
	case Medium:
		return "Medium"
	case Hard:
		return "Hard"
	default:
		return "Unknown:" + strconv.Itoa(int(qsl))
	}
}

type Question interface {
	GetID() string
	GetDifficulty() float32
	GetType() QuestionType
	GetQuery() string
	GetAnswer() string
}

type BaseQuestion struct {
	QuestionID string
	Difficulty float32
	Type       QuestionType
	Query      string
	Answer     string
}

func (bq *BaseQuestion) GetID() string {
	return bq.QuestionID
}

func (bq *BaseQuestion) GetDifficulty() float32 {
	return bq.Difficulty
}

func (bq *BaseQuestion) GetType() QuestionType {
	return bq.Type
}

func (bq *BaseQuestion) GetQuery() string {
	return bq.Query
}

func (bq *BaseQuestion) GetAnswer() string {
	return bq.Answer
}

func (bq *BaseQuestion) SetID(id string) {
	bq.QuestionID = id
}

func (bq *BaseQuestion) SetDifficulty(difficulty float32) {
	bq.Difficulty = difficulty
}

func (bq *BaseQuestion) SetType(qType QuestionType) {
	bq.Type = qType
}

type QuestionSet struct {
	ID        string
	Level     QuestionSetLevel
	Questions []Question
	Shuffle   bool
}

// seedHash generates a numeric seed from the seed string using the MurmurHash3 algorithm.
func SeedHash(seed string) int64 {
	h := murmur3.New64()
	_, _ = h.Write([]byte(seed))
	return int64(h.Sum64())
}

func (qs *QuestionSet) ShuffleQuestions() {
	for i := len(qs.Questions) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		qs.Questions[i], qs.Questions[j] = qs.Questions[j], qs.Questions[i]
	}
}
