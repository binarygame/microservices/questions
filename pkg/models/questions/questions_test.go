package models_test

import (
	"testing"

	"github.com/spaolacci/murmur3"
	"github.com/stretchr/testify/assert"
	models "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

func TestBaseQuestion(t *testing.T) {
	bq := &models.BaseQuestion{}

	// Test SetID and GetID
	id := "test_id"
	bq.SetID(id)
	assert.Equal(t, id, bq.GetID())

	// Test SetDifficulty and GetDifficulty
	difficulty := float32(0.6)
	bq.SetDifficulty(difficulty)
	assert.Equal(t, difficulty, bq.GetDifficulty())

	// Test SetType and GetType
	qType := models.DecimalToBinary
	bq.SetType(qType)
	assert.Equal(t, qType, bq.GetType())

	// Test setting and getting Query
	query := "What is 10 in binary?"
	bq.Query = query
	assert.Equal(t, query, bq.GetQuery())

	// Test setting and getting Answer
	answer := "1010"
	bq.Answer = answer
	assert.Equal(t, answer, bq.GetAnswer())
}

func TestSeedHash(t *testing.T) {
	seed := "test_seed"
	expectedHash := murmur3.Sum64([]byte(seed))

	hash := models.SeedHash(seed)
	assert.Equal(t, int64(expectedHash), hash)
}
