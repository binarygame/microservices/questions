package model

import (
	"errors"

	"github.com/google/uuid"
	questionsv1 "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1"
	types "gitlab.com/binarygame/microservices/questions/pkg/models/question_types"
	models "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

var (
	/* question set specific */

	ErrProtoQuestionSetLevelInvalid = errors.New("question set level is invalid")
	ErrProtoQuestionSetIDInvalid    = errors.New("question set id is invalid")

	/* question specific */

	ErrProtoQuestionTypeInvalid = errors.New("question type is invalid")
)

var ProtoLevelMap = map[questionsv1.DifficultyLevel]models.QuestionSetLevel{
	questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_UNSPECIFIED: models.Unknown,
	questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY:        models.Easy,
	questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_MEDIUM:      models.Medium,
	questionsv1.DifficultyLevel_DIFFICULTY_LEVEL_HARD:        models.Hard,
}

type questionTypeHandler func(*questionsv1.Question) models.Question

var questionTypeHandlers = map[questionsv1.QuestionType]questionTypeHandler{
	questionsv1.QuestionType_QUESTION_TYPE_DECIMAL_TO_BINARY: decimalToBinaryHandler,
	questionsv1.QuestionType_QUESTION_TYPE_BINARY_TO_DECIMAL: binaryToDecimalHandler,
}

func decimalToBinaryHandler(q *questionsv1.Question) models.Question {
	if q.Type != questionsv1.QuestionType_QUESTION_TYPE_DECIMAL_TO_BINARY {
		return nil
	}
	return &types.DecimalToBinary{
		BaseQuestion: models.BaseQuestion{
			QuestionID: q.Id,
			Type:       models.DecimalToBinary,
			Answer:     q.Answer,
			Query:      q.Query,
		},
	}
}

func binaryToDecimalHandler(q *questionsv1.Question) models.Question {
	if q.Type != questionsv1.QuestionType_QUESTION_TYPE_BINARY_TO_DECIMAL {
		return nil
	}
	return &types.BinaryToDecimal{
		BaseQuestion: models.BaseQuestion{
			QuestionID: q.Id,
			Type:       models.BinaryToDecimal,
			Answer:     q.Answer,
			Query:      q.Query,
		},
	}
}

// Converts protobuf's generated question set to internal model question set.
//
// TODO: Add question difficulty to protobuf, currently missing
func ConvertProtoQuestionSet(protoQuestionSet *questionsv1.QuestionSet) (*models.QuestionSet, error) {
	if err := uuid.Validate(protoQuestionSet.Id); err != nil {
		return nil, ErrProtoQuestionSetIDInvalid
	}

	level, found := ProtoLevelMap[protoQuestionSet.Level]
	if !found {
		return nil, ErrProtoQuestionSetLevelInvalid
	}

	questions := make([]models.Question, len(protoQuestionSet.Questions))
	for i, q := range protoQuestionSet.Questions {
		f, ok := questionTypeHandlers[q.Type]
		if !ok {
			return nil, ErrProtoQuestionTypeInvalid
		}
		question := f(q)
		if question == nil {
			return nil, ErrProtoQuestionTypeInvalid
		}
		questions[i] = question
	}
	return &models.QuestionSet{
		ID:        protoQuestionSet.Id,
		Level:     level,
		Questions: questions,
	}, nil
}
