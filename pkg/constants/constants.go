package constants

import "time"

// app configs
const (
	ExpireDuration             = 2 * time.Hour
	MaxAllowedQuestionsPerRoom = 30
	MaxBitsNumber              = 10
	MaxDecimalNumber           = 255
	NumGeneratedQuestions      = 1000
)

// telemetry configs
const (
	NamespaceKey               = "gitlab.com/binarygame/microservices/questions"
	QuestionsCreatedCounterKey = "questions_created"
	SetsCreatedCounterKey      = "sets_created"
)
