package errdefs

import "errors"

var (
	ErrQuestionDifficultyOutOfRange = errors.New("the question's difficulty value is outside the allowed range")
	ErrQuestionCountOutOfRange      = errors.New("the question count value is outside the allowed range")
	ErrDatabaseFailed               = errors.New("an error occurred while connecting with Redis")
	ErrQuestionDifficultyNotFound   = errors.New("difficulty not found in question map")
	ErrQuestionTypeNotFound         = errors.New("type not found in question map")
	ErrQuestionTypeUnknown          = errors.New("unknown question type")
	ErrQuestionSetIDEmpty           = errors.New("received empty question set id")
	ErrNoQuestionsFound             = errors.New("no questions found")
	ErrQuestionSetNotFound          = errors.New("question set not found")
)
